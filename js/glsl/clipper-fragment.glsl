precision highp float;
precision highp int;
uniform vec3 color;

uniform vec3 tool_domain_bounds_min;
uniform vec3 tool_domain_bounds_max;

varying vec4 vertex_pos;

void main(void) {
    float alpha = 0.1;
    float border_width = 0.001;
    if(vertex_pos.x < (tool_domain_bounds_min.x-border_width) || vertex_pos.x > (tool_domain_bounds_max.x+border_width) ||
       vertex_pos.y < (tool_domain_bounds_min.y-border_width) || vertex_pos.y > (tool_domain_bounds_max.y+border_width) ||
       vertex_pos.z < (tool_domain_bounds_min.z-border_width) || vertex_pos.z > (tool_domain_bounds_max.z+border_width)){
        discard;
    }else if(vertex_pos.x > (tool_domain_bounds_min.x-border_width) && vertex_pos.x < (tool_domain_bounds_min.x+border_width) || 
             vertex_pos.x > (tool_domain_bounds_max.x-border_width) && vertex_pos.x < (tool_domain_bounds_max.x+border_width) ||
             vertex_pos.y > (tool_domain_bounds_min.y-border_width) && vertex_pos.y < (tool_domain_bounds_min.y+border_width) || 
             vertex_pos.y > (tool_domain_bounds_max.y-border_width) && vertex_pos.y < (tool_domain_bounds_max.y+border_width) ||
             vertex_pos.z > (tool_domain_bounds_min.z-border_width) && vertex_pos.z < (tool_domain_bounds_min.z+border_width) || 
             vertex_pos.z > (tool_domain_bounds_max.z-border_width) && vertex_pos.z < (tool_domain_bounds_max.z+border_width)){
        alpha = 1.0;
    }

    gl_FragColor = vec4(color,alpha);
}