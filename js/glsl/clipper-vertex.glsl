precision highp float;
precision highp int;

uniform mat4 modelMatrix;
uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;

attribute vec3 position;

varying vec4 vertex_pos;

void main(void) {
    vertex_pos = modelMatrix * vec4(position,1.0);
    gl_Position = projectionMatrix *
                modelViewMatrix *
                vec4(position,1.0);
}
