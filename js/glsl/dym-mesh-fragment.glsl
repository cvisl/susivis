uniform vec3 clip_normal;
uniform vec3 clip_origin;
uniform int clipping;
uniform float a;

varying vec4 vertex_color;
varying vec3 vertex_position_eye;
varying vec3 vertex_position;
varying vec2 vertex_uv;
varying vec3 vertex_normal;

varying float vertex_visibility;

void main(void) {
    #ifdef USE_RGB
        gl_FragColor = vertex_color;
    #else
    vec4 color;

    float clip_plane_dot = dot(normalize(clip_normal), normalize(vertex_position-clip_origin));

    if(vertex_visibility < 0.9 || (clip_plane_dot > 0.0 && clipping == 1) //|| 
        //(vertex_position.x < -0.5 || vertex_position.x > 0.5 || 
        // vertex_position.y < -0.5 || vertex_position.y > 0.5 ||
        // vertex_position.z < -0.5 || vertex_position.z > 0.5)
        ){
        discard;
    }
#ifdef AS_LINE
    color = vertex_color;
#else

#ifdef USE_FLAT
    vec3 fdx = dFdx( vertex_position_eye );
    vec3 fdy = dFdy( vertex_position_eye );
    vec3 normal = normalize( cross( fdx, fdy ) );
#else
    vec3 normal = vertex_normal;
#endif
    float diffuse = dot( normal, vec3(0.0,0.0,1.0) );

    color = vec4( clamp(diffuse, 0.2, 1.) * vertex_color.rgb, a);
#endif // AS_LINE

    gl_FragColor = color;
    //gl_FragColor = vec4(vertex_normal, 1.0);
    #endif // USE_RGB
}
