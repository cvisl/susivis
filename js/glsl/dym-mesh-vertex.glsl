uniform mat4 domain_matrix;

varying vec4 vertex_color;
varying vec3 vertex_position_eye;
varying vec3 vertex_position;
varying float vertex_visibility;
varying vec3 vertex_normal;

attribute vec4 color;

attribute float vert_visibility;



void main(void) {
    vec4 pos = domain_matrix * vec4( position, 1.0 );
    vertex_position = pos.xyz;
    gl_Position = projectionMatrix *
                modelViewMatrix *
                pos;
    vertex_position_eye = ( modelViewMatrix * pos ).xyz;
    vertex_normal = normalMatrix*normal;
    vertex_visibility = vert_visibility;


    #ifdef USE_RGB
        vertex_color = vec4(pos.xyz + vec3(0.5, 0.5, 0.5), 1.0);
    #else
        vertex_color = color;
    #endif
}
