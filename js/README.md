IPywidgets for suspension simulation visualizations. These widgets are purely made as extension of the IPyvolume package, they might in later stage be added to the IPyvolume package if they are deemed suitable by the original author of IPyvolume.

Package Install
---------------

**Prerequisites**
- [node](http://nodejs.org/)

```bash
npm install --save susivis
```