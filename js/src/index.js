// Export widget models and views, and the npm package version number.
var _ = require('underscore')
module.exports = _.extend({}, require('./dymmesh.js'), require('./tools.js'));
module.exports['version'] = require('../package.json').version;
