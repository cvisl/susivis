var _ = require('underscore')
var widgets = require('@jupyter-widgets/base');
var THREE = require('three')
var pythreejs = require('jupyter-threejs');

require("./three/TransformControls.js")

var ToolModel = pythreejs.Object3DModel.extend({
    initialize: function() {
        pythreejs.Object3DModel.prototype.initialize.apply(this, arguments);
        this.initPromise.bind(this).then(this.initialize_tool_model);
    },
    initialize_tool_model: function() {
        this.renderer = null;
        this.material = undefined;
        this.mesh = undefined;

        this.tool_domain_bounds = {
            min:new THREE.Vector3(-0.5,-0.5,-0.5),
            max:new THREE.Vector3(0.5,0.5,0.5),
        }

        this.domain_matrix = new THREE.Matrix4();

        // Call basic render function for 3D interactive tools
        this.create_material();  
        this.create_mesh();
        this.setup_tool_callbacks();

        this.on("change:visible", this.on_change_visibility, this)
    },
    set_renderer: function (renderer) {
        this.renderer = renderer;
    },
    create_material: function() {
        // Override function
    },
    create_mesh: function() {
        // Override function
    },
    setup_tool_callbacks: function() {
        // Override function
    },
    set_visibility: function () {
        // Override function
    },
    on_change_visibility: function () {
        this.set_visibility(this.get('visible'));
    },
    set_limits: function(limits) {
        // var xptp = limits.xlim[1] - limits.xlim[0]
        // var yptp = limits.ylim[1] - limits.ylim[0]
        // var zptp = limits.zlim[1] - limits.zlim[0]

        // var domainscale = Math.max(Math.max(xptp,yptp),zptp)

        // this.domain_matrix.identity()
        //                         .multiply((new THREE.Matrix4()).makeScale(1/domainscale,1/domainscale,1/domainscale))
        //                         .multiply((new THREE.Matrix4()).makeTranslation(-(limits.xlim[0]+xptp/2),-(limits.ylim[0]+yptp/2),-(limits.zlim[0]+zptp/2)));                                

        // this.tool_domain_bounds.min.set(limits.xlim[0],limits.ylim[0],limits.zlim[0]).applyMatrix4(this.domain_matrix)
        // this.tool_domain_bounds.max.set(limits.xlim[1],limits.ylim[1],limits.zlim[1]).applyMatrix4(this.domain_matrix)
    },
    defaults: function() {
        return _.extend(pythreejs.Object3DModel.prototype.defaults(), {
            _model_name : 'ToolModel',
            _model_module : 'susivis',
            visible: true,
        })
    }
});

var ClipperModel = ToolModel.extend({
    initialize: function() {
        this.model_class = 'tool';
        ToolModel.prototype.initialize.apply(this, arguments);
        this.initPromise.bind(this).then(this.initialize_clipper_model);
    },
    initialize_clipper_model: function() {
        // Add additional render functions needed for this tool
        //this.create_control(); // moved to set parent renderer
        //console.log('initialize later when renderer is known');
        window.last_tool = this;
    },
    set_renderer: function (renderer) {
        this.renderer = renderer;
        this.create_control();
    },
    create_material: function () {
        this.material = new THREE.RawShaderMaterial({
            uniforms: {
                tool_domain_bounds_min: { type: "vec3", value: this.tool_domain_bounds.min},
                tool_domain_bounds_max: { type: "vec3", value: this.tool_domain_bounds.max},
                color: { type: "vec3", value: new THREE.Color(this.get("color"))},
            },
            side:THREE.DoubleSide,
            transparent: true,
            depthWrite: true, 
            depthTest: true,
            blending: THREE.NormalBlending,
            vertexShader: require('raw-loader!../glsl/clipper-vertex.glsl'),
            fragmentShader: require('raw-loader!../glsl/clipper-fragment.glsl'),
            visible: this.get("visible")
        })
    },
    create_mesh: function () {
        // 1.8 > longest length of unit cube so will always be outside domain boundaries
        var geometry = new THREE.PlaneGeometry( 2.0*1.8, 2.0*1.8); 
        this.mesh = new THREE.Mesh( geometry, this.material );
        this.mesh.name = 'clipper_plane';
        this.obj.add(this.mesh);
    },
    create_control: function () {
        this.clipper_control = new THREE.TransformControls( this.renderer.camera, this.renderer.renderer.domElement );
        this.clipper_control.setSize(0.5)
        this.clipper_control.attach(this.mesh);
        this.obj.add( this.clipper_control );

        this.set_clipper_control_parameters();
        this.clipper_control.addEventListener( 'change', _.bind(this.update_control, this) );
    },
    setup_tool_callbacks: function() {
        this.on("change:color change:clip_normal change:clip_origin",   this.on_change, this)
    },
    set_visibility: function(visible) {
        this.material.visible = visible;
        if(!visible) this.clipper_control.detach();
        else this.clipper_control.attach(this.mesh)
    },
    on_change: function() {
        this.set_clipper_control_parameters();
    },
    key_handle: function(evtObj) {
        if(evtObj.ctrlKey && evtObj.shiftKey && evtObj.altKey){
            if(evtObj.keyCode == 84) {  // t for translate
                this.clipper_control.setMode('translate')
            }
            if(evtObj.keyCode == 82) {  // r for rotate
                this.clipper_control.setMode('rotate')
            }
        }
    },
    set_clipper_control_parameters: function(){
        var color = this.get('color');
        var clip_origin = this.get('clip_origin');
        var clip_normal = this.get('clip_normal');
        this.clipper_control.object.position.set(clip_origin[0],clip_origin[1],clip_origin[2]);
        this.clipper_control.object.quaternion.setFromUnitVectors((new THREE.Vector3(0,0,1)),(new THREE.Vector3(clip_normal[0],clip_normal[1],clip_normal[2])));
        this.clipper_control.update();
    },
    update_control: function(){
        this.clipper_control.update();

        if(this.clipper_control.object.position.x < this.tool_domain_bounds.min.x)
            this.clipper_control.object.position.x = this.tool_domain_bounds.min.x;
        else if(this.clipper_control.object.position.x > this.tool_domain_bounds.max.x)
            this.clipper_control.object.position.x = this.tool_domain_bounds.max.x;

        if(this.clipper_control.object.position.y < this.tool_domain_bounds.min.y)
            this.clipper_control.object.position.y = this.tool_domain_bounds.min.y;
        else if(this.clipper_control.object.position.y > this.tool_domain_bounds.max.y)
            this.clipper_control.object.position.y = this.tool_domain_bounds.max.y;

        if(this.clipper_control.object.position.z < this.tool_domain_bounds.min.z)
            this.clipper_control.object.position.z = this.tool_domain_bounds.min.z;
        else if(this.clipper_control.object.position.z > this.tool_domain_bounds.max.z)
            this.clipper_control.object.position.z = this.tool_domain_bounds.max.z;

        this.clipper_control.update();

        this.set('clip_origin', this.clipper_control.object.position.toArray());
        this.set('clip_normal', (new THREE.Vector3(0,0,1)).applyQuaternion(this.clipper_control.object.quaternion).toArray());
        this.renderer.update();
    },
    defaults: function() {
        return _.extend(ToolModel.prototype.defaults(), {
            _model_name : 'ClipperModel',
            _model_module : 'susivis',
            color: "red",
            clip_origin: [0,0,0],
            clip_normal: [0,0,1],
        })
    }
});



module.exports = {
    ToolModel:ToolModel,
    ClipperModel:ClipperModel
}
