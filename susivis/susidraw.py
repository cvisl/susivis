from ipyvolume import pylab
import numpy as np
from matplotlib import cm
from matplotlib.colors import ListedColormap
from .visutils import *
import vtk
from vtk.util.numpy_support import vtk_to_numpy
from .widgets import DymMesh, Clipper
import ipywidgets

def mesh(vertices=[], triangles=[], mesh_point_data=[], color=cm.viridis, custom_data_range=None, vert_visibility=None, smooth=False):
    """ This function draws a mesh based on a set of vertex locations, [[x,y,z],...] and 
    a set of vertex indices, [[vert1,vert2,vert3],...], that define the triangles.

    :param vertices: a numpy array of 3D vertex locations (mesh points), [[x,y,z],...] with shape (number vertices, 3) 
                     or temporal [[[x,y,z],...]...] with shape (timesteps, number vertices, 3) 
    :param triangles: numpy array with indices referring to the vertices, defining the triangles, with shape (M, 3)
    :param mesh_point_data: the mesh point data which the color map will be applied to
    :param color: Either a matplotlib color map, which will be applied to normalized mesh_point_data, or a single color, e.g. 'red'
    :param texture: {texture}
    :return: :any:`Mesh`
    """

    if not isinstance(triangles, np.ndarray):
        triangles = np.array(triangles)
    if not isinstance(vertices, np.ndarray):
        vertices = np.array(vertices)

    # 2 dimension is one single time frame, more time frames the ndims are either 3 or 1 (when number of triangles is not equal over time)
    if triangles.ndim == 2:
        triangles = [triangles]

    vertposx = []
    vertposy = []
    vertposz = []
    colors = []

    # Single time step or multiple
    # 2 dimension is one single time frame, more time frames the ndims are either 3 or 1 (when number of triangles is not equal over time, then its one array with arrays which equals 1 dim)
    if vertices.ndim == 2:  
        if isinstance(color, ListedColormap):
            colors = normalize_data_to_color(mesh_point_data, color);
        else:
            colors = color 

        if vert_visibility is None:
            vert_visibility = np.ones(vertices.shape[0]).astype(np.uint8)

        vertposx,vertposy,vertposz = np.transpose(vertices)
    else:
        if isinstance(color, ListedColormap):
            for mpd in mesh_point_data:
                colors.append(normalize_data_to_color(mpd, color));
        else:
            colors = color

        if vert_visibility is None:
            vert_visibility = []
            for v in vertices:
                vert_visibility.append(np.ones(v.shape[0]).astype(np.uint8))

        for verts in vertices:
            vpx,vpy,vpz = np.transpose(verts)
            vertposx.append(vpx)
            vertposy.append(vpy)
            vertposz.append(vpz)

    return plot_dymmesh(vertposx,vertposy,vertposz,triangles=triangles, color=colors, vert_visibility=vert_visibility, smooth=smooth)

def vectors(pos=[], vectors=[], vector_scale=10, solid_vectors=False, selected=None, color='white'):
    """ Draws a set of vectors on the positions given. FOR NOW THIS IS NOT ANIMATED. Also the positions and vectors are disconnected
    so when they are changed it will not propogate to the visualization.

    :param pos: A numpy array of vector positions.
    :param vectors: A numpy array of vectors to be visualized.
    :param vector_scale: a visual scaling factor for the vectors.
    :param selected: Set of selected vectors, this comes from the interactive tool from ipyvolume.
    :param color: string giving the name of the color, e.g. 'white', 'red', 'green', etc.
    :return: :any:`Scatter`
    """

    widget = None;
    vector_mags = np.linalg.norm(vectors, axis=1)
    sizes = np.zeros(len(vector_mags))

    if selected is not None:
        sizes.put(selected, vector_mags) # size 0.0 when not selected
    else:
        sizes = vector_mags 

    #if solid_vectors:
    x,y,z = np.transpose(pos)
    u,v,w = np.transpose(vectors)
    widget = pylab.quiver(x,y,z,u,v,w,
        size=sizes*vector_scale, 
        size_selected=sizes*vector_scale, 
        color=color, 
        color_selected=color,
        marker='arrow',
        selected=selected)    
    # else:    
    #     vector_end_points = pos + (vectors*vector_scale)
    #     vectorlines = np.stack((np.arange(len(pos)), np.arange(len(pos))+len(vector_end_points)), axis=-1)

    #     pos = np.append(pos, vector_end_points, axis=0)

    #     x,y,z = np.transpose(pos)

    #     widget = plot_dymmesh(x,y,z,lines=vectorlines, color='white')
    
    return widget

def static_stl_mesh(stl_file_path, smooth=True,color='grey', alpha = 1.0):
    
    stl_rdr = vtk.vtkSTLReader()
    stl_rdr.SetFileName(stl_file_path)
    stl_rdr.Update()

    stl_data = stl_rdr.GetOutput()

    vertices = vtk_to_numpy(stl_data.GetPoints().GetData())

    polys = vtk_to_numpy(stl_data.GetPolys().GetData())
    polys = np.delete(polys, slice(None, None, 4))

    vertposx,vertposy,vertposz = np.transpose(vertices)

    return plot_dymmesh(vertposx,vertposy,vertposz, triangles=[polys], color=color, alpha=alpha)

def clip(objs, clip_origin=[0.0,0.0,0.0], clip_normal=[1.0,0.0,0.0], color='red'):
    """Draws a clipping plane for clipping meshes given in obj

    :param object: :any:`Mesh` object ,  {x2d}
    :param slice_origin: origin of clipping plane
    :param slice_normal: normal of clipping plane
    :param color: color of clipping plane
    :return: :any:`Clipper`
    """

    if not isinstance(objs, (list, tuple)):
        objs = [objs]

    fig = pylab.gcf()
    clipper = Clipper(objs=objs, clip_origin=clip_origin, clip_normal=clip_normal, color=color)

    for o in objs:
        # eventhough it is bi-directional I had to define both directions to also change the kernel parameters :/... bug?
        ipywidgets.jslink((clipper, 'clip_origin'), (o, 'clip_origin'))
        ipywidgets.jslink((clipper, 'clip_normal'), (o, 'clip_normal'))
        ipywidgets.jslink((o, 'clip_origin'), (clipper, 'clip_origin'))
        ipywidgets.jslink((o, 'clip_normal'), (clipper, 'clip_normal'))
        o.clipping = True;

    
    fig.object3D_models = fig.object3D_models + [clipper]
    return clipper

def iso_surface_mesh(volume=None, iso_values=None, colors=None, vtk=True, smooth=True):
    if iso_values is None:
        print('Please isosurface values in either a list or one singular value')

    if volume is None:
        print('No volume provided, please provide a volume')

    #if not isinstance(volume, np.ndarray):
    #    volume = np.array(volume)

    if not isinstance(iso_values, list):
        iso_values = [iso_values]

    if colors is None:
        colors = []
        for i in range(len(iso_values)):
            colors.append(cm.tab10((i%10.0)/10.0));

    confFunc = None
    if vtk:
        confFunc = isosurfacevtk
    else:
        confFunc = isosurfacesk

    meshes = []


    for i, iso_value in enumerate(iso_values):
        polys = []
        
        vertposx = []
        vertposy = []
        vertposz = []

        normals = []

        if volume.ndim == 4: # if 4 dimensions it means multiple timesteps
            for vol in volume:
                vx,vy,vz,p,n = confFunc(vol,iso_value);    
                vertposx.append(vx)
                vertposy.append(vy)
                vertposz.append(vz)
                polys.append(p)
                normals.append(n)
        else:
            vertposx,vertposy,vertposz,polys,normals = confFunc(volume,iso_value);
            polys=[polys]

        meshes.append(plot_dymmesh(vertposx,vertposy,vertposz,polys,color=colors[i],smooth=smooth,normals=normals))

    return meshes

def plot_dymmesh(x, y, z, triangles=None, color='red', u=None, v=None, texture=None, vert_visibility=1.0, smooth=True, normals=None, alpha=1.0):
    """Draws a triangle mesh defined by a coordinate and triangle indices

    The following example plots a rectangle in the z==2 plane, consisting of 2 triangles:

    >>> plot_trisurf([0, 0, 3., 3.], [0, 4., 0, 4.], 2,
           triangles=[[0, 2, 3], [0, 3, 1]])

    Note that the z value is constant, and thus not a list/array. For guidance, the triangles
    refer to the vertices in this manner::

        ^ ydir
        |
        2 3
        0 1  ---> x dir

    Note that if you want per face/triangle colors, you need to duplicate each vertex.


    :param x: {x}
    :param y: {y}
    :param z: {z}
    :param triangles: numpy array with indices referring to the vertices, defining the triangles, with shape (M, 3)
    :param lines: numpy array with indices referring to the vertices, defining the lines, with shape (K, 2)
    :param color: {color}
    :param u: {u}
    :param v: {v}
    :param texture: {texture}
    :return: :any:`Mesh`
    """
    fig = pylab.gcf()

    if vert_visibility is None:
            vert_visibility = np.ones(x.shape[0]).astype(np.uint8)

    #Check if unequal multi frame array
    if isinstance(x[0], np.ndarray):
        for i in range(len(x)):
            pylab._grow_limits(np.array(x[i]).reshape(-1), np.array(y[i]).reshape(-1), np.array(z[i]).reshape(-1))
            max_limits = [min(fig.xlim[0],fig.ylim[0],fig.zlim[0]),max(fig.xlim[1],fig.ylim[1],fig.zlim[1])]
            pylab.xlim(*pylab._grow_limit(fig.xlim, max_limits))
            pylab.ylim(*pylab._grow_limit(fig.ylim, max_limits))
            pylab.zlim(*pylab._grow_limit(fig.zlim, max_limits))
    else:
        pylab._grow_limits(np.array(x).reshape(-1), np.array(y).reshape(-1), np.array(z).reshape(-1))
        max_limits = [min(fig.xlim[0],fig.ylim[0],fig.zlim[0]),max(fig.xlim[1],fig.ylim[1],fig.zlim[1])]
        pylab.xlim(*pylab._grow_limit(fig.xlim, max_limits))
        pylab.ylim(*pylab._grow_limit(fig.ylim, max_limits))
        pylab.zlim(*pylab._grow_limit(fig.zlim, max_limits))

    tris = []
    if triangles is not None:
        for i, triset in enumerate(triangles):
            tris.append(np.array(triset).astype(dtype=np.uint32)) # Dirty copying because somehow it didn't want to change type, needs to change!
    mesh = DymMesh(x=x, y=y, z=z, 
        triangles=tris,  
        color=color, 
        u=u, v=v, 
        texture=texture, 
        vert_visibility=vert_visibility,
        smooth=smooth,
        normals=normals,
        a=alpha)

    fig.object3D_models = fig.object3D_models + [mesh]
    return mesh